//
//  Photo.swift
//  Zadatak
//
//  Created by Strahinja Grubor on 3/5/19.
//  Copyright © 2019 Strahinja Grubor. All rights reserved.
//

class Photo{
    
    var id: Int?
    var albumId: Int?
    var title: String?
    var url: String?
    var thumbnailUrl: String?
    
    init(id: Int?, albumId: Int?, title: String?, url: String?, thumbnailUrl: String?) {
        
        self.id = id
        self.albumId = albumId
        self.title = title
        self.url = url
        self.thumbnailUrl = thumbnailUrl
    }
    
    
}
