//
//  ViewControllerTableViewCell.swift
//  Zadatak
//
//  Created by Strahinja Grubor on 3/5/19.
//  Copyright © 2019 Strahinja Grubor. All rights reserved.
//

import UIKit

class ViewControllerTableViewCell: UITableViewCell {
 
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var url: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
