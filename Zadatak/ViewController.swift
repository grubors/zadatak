//
//  ViewController.swift
//  Zadatak
//
//  Created by Strahinja Grubor on 3/5/19.
//  Copyright © 2019 Strahinja Grubor. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var photos = [Photo]()
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let photo: Photo
        photo = photos[indexPath.row]
        
        UIApplication.shared.open(URL(string: photo.url!)! as URL, options: [:], completionHandler: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return photos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ViewControllerTableViewCell
        
        let photo: Photo
        photo = photos[indexPath.row]
        
        cell.title.text = photo.title
        cell.url.text = photo.url
        
        Alamofire.request(photo.thumbnailUrl!).responseImage { response in
            
            if let image = response.result.value {
                cell.thumbnail.image = image
            }
        }
        return cell
    }
    
    
    @IBOutlet weak var tableViewImages: UITableView!
    
    let url = "https://jsonplaceholder.typicode.com/photos"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Alamofire.request(url).responseJSON { response in
            
            if let json = response.result.value {
                
                let photosArray : NSArray = json as! NSArray
                
                for i in 0 ..< photosArray.count {
                    
                    self.photos.append(Photo(
                        id: (photosArray[i] as AnyObject).value(forKey: "id") as? Int,
                        albumId: (photosArray[i] as AnyObject).value(forKey: "albumId") as? Int,
                        title:(photosArray[i] as AnyObject).value(forKey: "title") as? String,
                        url:(photosArray[i] as AnyObject).value(forKey: "url") as? String,
                        thumbnailUrl:(photosArray[i] as AnyObject).value(forKey: "thumbnailUrl") as? String
                    ))
                }
                
                self.tableViewImages.reloadData()
            }
 
        }
        
    }
}

